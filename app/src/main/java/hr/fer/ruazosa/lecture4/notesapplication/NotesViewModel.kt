package hr.fer.ruazosa.lecture4.notesapplication

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class NotesViewModel: ViewModel() {
    var listOfNotes = MutableLiveData<MutableList<Note>>()
    fun saveNote(note: Note) {
        NoteRepository.notes.add(note)
        listOfNotes.value = NoteRepository.notes
    }
    fun getNote(noteAtIndex: Int): Note {
        return NoteRepository.notes[noteAtIndex]
    }
    fun getNoteCount(): Int {
        return NoteRepository.notes.size
    }
}
package hr.fer.ruazosa.lecture4.notesapplication

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import hr.fer.ruazosa.lecture4.notesapplication.databinding.NoteCellBinding

class NotesAdapter(notesViewModel: NotesViewModel): RecyclerView.Adapter<NotesAdapter.NotesViewHolder>() {
    var notesViewModel = notesViewModel
    var _binding: NoteCellBinding? = null
    private val binding get() = _binding!!

    class NotesViewHolder(noteView: View): RecyclerView.ViewHolder(noteView) {
        var noteDateTextView: TextView = noteView.findViewById(R.id.noteDateTextViewId)
        var noteTitleTextView: TextView = noteView.findViewById(R.id.noteTitleTextViewId)
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NotesViewHolder {
        val context = parent.context
        val inflater = LayoutInflater.from(context)
        _binding = NoteCellBinding.inflate(inflater)
        val notesView = binding.root
        return NotesViewHolder(notesView)
    }

    override fun onBindViewHolder(holder: NotesViewHolder, position: Int) {
        val note = notesViewModel.getNote(position)
        holder.noteDateTextView.text = note.noteDate.toString()
        holder.noteTitleTextView.text = note.noteTitle.toString()
    }
    override fun getItemCount(): Int {
        return notesViewModel.getNoteCount()
    }
}
package hr.fer.ruazosa.lecture4.notesapplication

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import hr.fer.ruazosa.lecture4.notesapplication.databinding.FragmentNoteDetailsBinding
import java.util.Date


class NoteDetailsFragment : Fragment() {

    private var _binding: FragmentNoteDetailsBinding? = null
    private val binding get() = _binding!!
    private val sharedViewModel: NotesViewModel by activityViewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentNoteDetailsBinding.inflate(inflater, container, false)
        val view = binding.root
        binding.saveNoteButtonId.setOnClickListener {
            val note = Note(noteDate = Date(), noteTitle = binding.noteDetailsTitleEditTextId.text.toString(),
                noteDescription = binding.noteDetailsNoteDescriptionEditTextId.text.toString())
            sharedViewModel.saveNote(note)
            val navigationController = findNavController()
            navigationController.popBackStack()
        }
        return view
    }
}